//
//  MemoViewController.h
//  SillyBox
//
//  Created by 재훈 신 on 2015. 8. 29..
//  Copyright (c) 2015년 재훈 신. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"

@interface MemoViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextView *TextView;
@property (strong, nonatomic) IBOutlet UIButton *startWeekButton;
@property (strong, nonatomic) IBOutlet UILabel *endWeekLabel;
@property (strong, nonatomic) IBOutlet UIView *popupView;
@property (strong, nonatomic) DBManager *dbManager;

- (IBAction)OnClickSaveButton:(id)sender;
- (IBAction)OnClickStartWeekButton:(id)sender;
- (void) ChangeButtonsDate:(NSString*)week;

@end