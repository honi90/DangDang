//
//  EnterWeekPopupViewController.m
//  SillyBox
//
//  Created by 재훈 신 on 2015. 8. 29..
//  Copyright (c) 2015년 재훈 신. All rights reserved.
//

#import "EnterWeekPopupViewController.h"
#import "SingletoneObject.h"

@interface EnterWeekPopupViewController ()

@end

@implementation EnterWeekPopupViewController
@synthesize DueDateTextField;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}


- (IBAction)OnClickOKButton:(id)sender {
    
    if (DueDateTextField.text.length)
    {
        [[[SingletoneObject GetSingletoneObject] GetPopupView] setHidden:true];;
        [[[SingletoneObject GetSingletoneObject] MemoViewControoler] ChangeButtonsDate:DueDateTextField.text];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"날짜를 넣어주세요" delegate:self cancelButtonTitle:@"Done" otherButtonTitles:nil];
        [alert show];
    }
}

@end
