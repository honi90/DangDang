//
//  MemoReaderViewController.m
//  SillyBox
//
//  Created by 재훈 신 on 2015. 8. 29..
//  Copyright (c) 2015년 재훈 신. All rights reserved.
//

#import "MemoReaderViewController.h"

@interface MemoReaderViewController ()

@end

@implementation MemoReaderViewController
@synthesize dbManager, tableID, TextView;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"MemoryDB.sql"];
    [self SetTextView];
}

- (void) SetTextView {
    NSString *query = [NSString stringWithFormat:@"select * from MemoryInfo where ID =%d", tableID];
    
    // Load the relevant data.
    NSArray *results = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    NSString *content = [[results objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"content"]];
    
    [TextView setText:content];
    TextView.editable = false;
}

- (IBAction)OnClickFlyingButton:(id)sender {
    NSString *query = [NSString stringWithFormat:@"delete from MemoryInfo where id = %d", tableID];
    
    [self.dbManager executeQuery:query];
    
    if (self.dbManager.affectedRows != 0) {
        NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
        
        [self.navigationController popViewControllerAnimated:YES];
    }
    else{
        NSLog(@"Could not execute the query.");
    }
}

- (IBAction)onClickKeepButton:(id)sender {
    NSString *query = [NSString stringWithFormat:@"update MemoryInfo set open_day='%@'", [self GetOpenDueDate]];

    // Execute the query.
    [self.dbManager executeQuery:query];
    
    if (self.dbManager.affectedRows != 0) {
        NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
    }
    else{
        NSLog(@"Could not execute the query.");
    }
}

- (NSString *) GetOpenDueDate {
    NSDate *currentDate = [NSDate date];
    
    NSDateComponents *dateComponents = [NSDateComponents new];
  
    dateComponents.day = 7;
    
    NSDate *dueDate = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:currentDate options:0];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy.M.d";
    
    NSString *formattedDate = [formatter stringFromDate:dueDate];
    return formattedDate;
}

- (NSString*) GetTodayDate {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy.M.d";
    NSString *formattedDate = [formatter stringFromDate:[NSDate date]];
    return formattedDate;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}



@end
