//
//  ViewController.m
//  SillyBox
//
//  Created by 재훈 신 on 2015. 8. 29..
//  Copyright (c) 2015년 재훈 신. All rights reserved.
//

#import "ViewController.h"
#import "DBManager.h"
#import "MemoReaderViewController.h"

@interface ViewController ()

@property (nonatomic, strong) DBManager *dbManager;
@property (nonatomic, strong) NSArray *memoryInfos;
@property (nonatomic) int tableIDValue;
- (void)loadData;

@end

@implementation ViewController
@synthesize ShowMessageButton;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"MemoryDB.sql"];
    [self loadData];
    [self SetShowMessageButton];
}

- (void) SaveMemoryInfoToDataBase {
    NSString *query = [NSString stringWithFormat:@"insert into Memoryinfo (content, saved_day, open_day) values('%@', '%@', '%@')", @"데브캠프를 하고 있었습니다.\n그런데 너무 힘들었어요. 우리는 게임을 만들기로 했지만... 당일에 열심히 개발하다가 새벽 1시에 기획을 뒤집었어요. 아 물론 새로운 기획도 없는 상태였어요.\n 그렇게 기획하다보니 새벽 세시가 넘었고... 그 때부터 기획서를 쓰기 시작했어요. 해커톤을 하면 다신 이렇게 하면 안되겠다 싶었어요.\n 그래도 이 앱 괜찮지 않아요?", @"2015.8.30", @"2015.8.30"];
    
    // Execute the query.
    [self.dbManager	executeQuery:query];
    
    // If the query was successfully executed then pop the view controller.
    if (self.dbManager.affectedRows != 0) {
        NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
    }
    else{
        NSLog(@"Could not execute the query.");
    }
}

- (void)SetShowMessageButton {
    [ShowMessageButton setEnabled:[self CheckOpenMessageIsExist]];
    
    if ([self CheckOpenMessageIsExist]) {
        [ShowMessageButton backgroundImageForState:UIControlStateNormal];
    }
    else {
        [ShowMessageButton backgroundImageForState:UIControlStateDisabled];
    }
}

- (NSString*) GetTodayDate {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy.M.d";
    NSString *formattedDate = [formatter stringFromDate:[NSDate date]];
    return formattedDate;
}

- (BOOL) CheckOpenMessageIsExist {
    
    NSString *query = [NSString stringWithFormat:@"select * from MemoryInfo where open_day ='%@'", [self GetTodayDate]];
    
    // Load the relevant data.
    NSArray *results = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    
    if (results.count > 0) {
        _tableIDValue = [[[results objectAtIndex:0] objectAtIndex:[self.dbManager.arrColumnNames indexOfObject:@"ID"]] intValue];
        NSLog(@"_tableIDValue : %d", _tableIDValue);
        return true;
    }
    else
        return false;
}

- (void) didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) loadData {
    // Form the query.
    NSString *query = @"select * from Memoryinfo";
    
    // Get the results.
    if (self.memoryInfos != nil) {
        self.memoryInfos = nil;
    }
    self.memoryInfos = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"MemoReaderViewController"]) {
        MemoReaderViewController *memoReaderVC = (MemoReaderViewController *)segue.destinationViewController;
        memoReaderVC.tableID = _tableIDValue;
    }
}

- (IBAction)OnClickMakeToday:(id)sender {
        [self SaveMemoryInfoToDataBase];
        [self SetShowMessageButton];
}

@end
