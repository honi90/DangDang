//
//  SingletoneObject.m
//  SocialCodi
//
//  Created by 재훈 신 on 2015. 6. 5..
//  Copyright (c) 2015년 재훈 신. All rights reserved.
//

#import "SingletoneObject.h"

@implementation SingletoneObject
@synthesize PopupViewController, MemoViewControoler;

+(SingletoneObject*)GetSingletoneObject {
    
    static SingletoneObject *sharedInstance = nil;
    
    if (sharedInstance == nil ) {
        @synchronized(self) {
            if (sharedInstance == nil ) {
                sharedInstance = [[SingletoneObject alloc]init];
            }
        }
    }
    
    return  sharedInstance;
}

- (void)SetPopupView:(UIView *)view {
    PopupViewController = view;
}

- (UIView*)GetPopupView {
    return PopupViewController;
}
@end
