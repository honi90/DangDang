//
//  MemoReaderViewController.h
//  SillyBox
//
//  Created by 재훈 신 on 2015. 8. 29..
//  Copyright (c) 2015년 재훈 신. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DBManager.h"

@interface MemoReaderViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextView *TextView;
@property (strong, nonatomic) DBManager *dbManager;
@property (nonatomic) int tableID;

- (IBAction)OnClickFlyingButton:(id)sender;
- (IBAction)onClickKeepButton:(id)sender;

@end


