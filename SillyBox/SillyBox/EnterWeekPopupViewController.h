//
//  EnterWeekPopupViewController.h
//  SillyBox
//
//  Created by 재훈 신 on 2015. 8. 29..
//  Copyright (c) 2015년 재훈 신. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EnterWeekPopupViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *DueDateTextField;

- (IBAction)OnClickOKButton:(id)sender;

@end
