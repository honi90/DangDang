//
//  SingletoneObject.h
//  SocialCodi
//
//  Created by 재훈 신 on 2015. 6. 5..
//  Copyright (c) 2015년 재훈 신. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "MemoViewController.h"

@interface SingletoneObject : NSObject {
    UIView *PopupViewController;
    MemoViewController *MemoViewControoler;
}

+(SingletoneObject *)GetSingletoneObject;

- (void)SetPopupView:(UIView*)view;
- (UIView*)GetPopupView;

@property (nonatomic, strong) UIView *PopupViewController;
@property (nonatomic, strong) MemoViewController *MemoViewControoler;
@end
