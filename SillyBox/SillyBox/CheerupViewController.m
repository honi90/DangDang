//
//  CheerupViewController.m
//  SillyBox
//
//  Created by 재훈 신 on 2015. 8. 30..
//  Copyright (c) 2015년 재훈 신. All rights reserved.
//

#import "CheerupViewController.h"

@interface CheerupViewController ()

@end

@implementation CheerupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated {
    [self performSelector:@selector(GoToHome) withObject:nil afterDelay:4.0f];
}

- (void)GoToHome {
    [_homeButton sendActionsForControlEvents: UIControlEventTouchUpInside];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
