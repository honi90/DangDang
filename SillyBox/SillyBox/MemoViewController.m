//
//  MemoViewController.m
//  SillyBox
//
//  Created by 재훈 신 on 2015. 8. 29..
//  Copyright (c) 2015년 재훈 신. All rights reserved.
//

#import "MemoViewController.h"
#import "SingletoneObject.h"

@interface MemoViewController ()

@end

@implementation MemoViewController
@synthesize TextView, startWeekButton, endWeekLabel, popupView, dbManager;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"MemoryDB.sql"];
    
    [[SingletoneObject GetSingletoneObject] SetPopupView:popupView];
    [[SingletoneObject GetSingletoneObject] setMemoViewControoler:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)OnClickSaveButton:(id)sender {
//    NSLog(@"%@", [self GetTodayDate]);;
    [self GetOpenDueDate];
    [self SaveMemoryInfoToDataBase];
}

- (int) ChangeWeekToDay {
    return [[startWeekButton.titleLabel text] intValue] * 7;
}

- (int) GetRandomValueOfDate {
    return arc4random() % 7 + [self ChangeWeekToDay];
}

- (NSString *) GetOpenDueDate {
    NSDate *currentDate = [NSDate date];

    NSDateComponents *dateComponents = [NSDateComponents new];
    dateComponents.day = [self GetRandomValueOfDate];
    
    NSDate *dueDate = [[NSCalendar currentCalendar] dateByAddingComponents:dateComponents toDate:currentDate options:0];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy.M.d";
    
    NSString *formattedDate = [formatter stringFromDate:dueDate];
    return formattedDate;
}

- (void) SaveMemoryInfoToDataBase {
    NSString *query = [NSString stringWithFormat:@"insert into Memoryinfo (content, saved_day, open_day) values('%@', '%@', '%@')", TextView.text, [self GetTodayDate], [self GetOpenDueDate]];
    
    // Execute the query.
    [self.dbManager	executeQuery:query];
    
    // If the query was successfully executed then pop the view controller.
    if (self.dbManager.affectedRows != 0) {
        NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
    }
    else{
        NSLog(@"Could not execute the query.");
    }
}

- (IBAction)OnClickStartWeekButton:(id)sender {
    //TODO : Change StartWeek and EndWeek.
    //EndWeek is calculated StartWeek + 7;
    popupView.hidden = false;
}

- (NSString*) GetTodayDate {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy.M.d";
    NSString *formattedDate = [formatter stringFromDate:[NSDate date]];
    return formattedDate;
}

- (void)ChangeButtonsDate:(NSString*)week {
    [startWeekButton setTitle:week forState:UIControlStateNormal];
    [endWeekLabel setText: [NSString stringWithFormat:@"%d",([week intValue] +1)]];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

@end
